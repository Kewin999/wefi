package com.begoml.common

import android.text.format.DateUtils

fun Long.formatDuration(): String = if (this < 60) {
    toString()
} else {
    DateUtils.formatElapsedTime(this)
}