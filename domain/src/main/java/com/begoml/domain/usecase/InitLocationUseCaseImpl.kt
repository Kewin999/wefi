package com.begoml.domain.usecase

import com.begoml.common.Failure
import com.begoml.common.Result
import com.begoml.domain.repository.MeetingRepository
import javax.inject.Inject

class InitLocationUseCaseImpl @Inject constructor(
    private val repository: MeetingRepository
) : InitLocationUseCase() {
    override suspend fun run(params: Any): Result<Failure, Any> {
        return repository.initLocation().result()
    }
}

abstract class InitLocationUseCase : UseCase<Any, Any>()