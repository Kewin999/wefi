package com.begoml.domain.usecase

import com.begoml.common.Failure
import com.begoml.common.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

abstract class UseCase<Type, in Params> where Type : Any {

    abstract suspend fun run(params: Params): Result<Failure, Type>

    open operator fun invoke(
        asyncScope: CoroutineScope,
        launchScope: CoroutineScope,
        params: Params,
        onResult: (Result<Failure, Type>) -> Unit = {}
    ) {
        val backgroundJob = asyncScope.async {
            run(params)
        }

        try {
            launchScope.launch { onResult(backgroundJob.await()) }
        } catch (error: Exception) {
            launchScope.launch { onResult(Result.Error(Failure.UnknownFailure(error))) }
        }
    }

    protected fun Result<Failure, Type>.result(): Result<Failure, Type> {
        return when (this) {
            is Result.Error -> this
            is Result.Success -> Result.Success(data)
        }
    }

}