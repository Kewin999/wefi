package com.begoml.domain.usecase

import com.begoml.common.Failure
import com.begoml.common.Result
import com.begoml.domain.model.SaveData
import com.begoml.domain.repository.MeetingRepository
import javax.inject.Inject

class SaveDataUseCaseImpl @Inject constructor(
    private val meetingRepository: MeetingRepository
) : SaveDataUseCase() {


    override suspend fun run(params: SaveData): Result<Failure, Any> {
        return meetingRepository.saveData(params).result()
    }
}


abstract class SaveDataUseCase : UseCase<Any, SaveData>()