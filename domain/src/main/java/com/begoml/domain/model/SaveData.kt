package com.begoml.domain.model

data class SaveData(
    val duration: Long = 0,
    val name: String = "",
    val description: String = ""
)