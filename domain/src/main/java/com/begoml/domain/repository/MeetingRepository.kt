package com.begoml.domain.repository

import com.begoml.common.Failure
import com.begoml.common.Result
import com.begoml.domain.model.SaveData

interface MeetingRepository {

    suspend fun saveData(params: SaveData): Result<Failure, Any>

    suspend fun initLocation(): Result<Failure, Any>
}