package com.begoml.data.model.dto

data class LocationDto(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
)