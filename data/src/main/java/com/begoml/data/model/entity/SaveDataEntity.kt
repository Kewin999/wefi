package com.begoml.data.model.entity

import com.begoml.data.model.dto.LocationDto

data class SaveDataEntity(
    val duration: Long = 0,
    val name: String = "",
    val description: String = "",
    val startLocation: LocationDto,
    val lastLocation: LocationDto
)