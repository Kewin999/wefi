package com.begoml.data.model.dto

data class SaveDataDto(
    val duration: String = "",
    val name: String = "",
    val description: String = ""
)