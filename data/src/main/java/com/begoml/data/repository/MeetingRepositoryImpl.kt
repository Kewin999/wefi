package com.begoml.data.repository

import android.content.Context
import com.begoml.common.Failure
import com.begoml.common.Result
import com.begoml.data.datastore.CacheDataStore
import com.begoml.data.datastore.RemoteDataStore
import com.begoml.data.model.dto.LocationDto
import com.begoml.data.model.entity.SaveDataEntity
import com.begoml.domain.model.SaveData
import com.begoml.domain.repository.MeetingRepository
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Tasks
import javax.inject.Inject


class MeetingRepositoryImpl @Inject constructor(
    private val remoteDataStore: RemoteDataStore,
    private val cacheDataStore: CacheDataStore,
    private val context: Context
) : MeetingRepository {


    private val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(
            context
        )
    }

    override suspend fun saveData(params: SaveData): Result<Failure, Any> {
        val locationDto = try {
            val task = Tasks.await(fusedLocationClient.lastLocation)
            LocationDto(
                latitude = task.latitude,
                longitude = task.longitude
            )
        } catch (e: Exception) {
            LocationDto()
        }

        val startLocation = cacheDataStore.locationDto ?: LocationDto()

        val entity = SaveDataEntity(
            name = params.name,
            description = params.description,
            duration = params.duration,
            lastLocation = locationDto,
            startLocation = startLocation
        )

        return remoteDataStore.sendData(entity)
    }

    override suspend fun initLocation(): Result<Failure, Any> {
        val locationDto = try {
            val task = Tasks.await(fusedLocationClient.lastLocation)
            LocationDto(
                latitude = task.latitude,
                longitude = task.longitude
            )
        } catch (e: Exception) {
            LocationDto()
        }
        cacheDataStore.locationDto = locationDto

        return Result.Success(Any())
    }
}