package com.begoml.data.datastore

import com.begoml.data.model.dto.LocationDto
import javax.inject.Inject

class CacheDataStoreImpl @Inject constructor() :
    CacheDataStore {

    override var locationDto: LocationDto? = null
}


interface CacheDataStore {
    var locationDto: LocationDto?
}