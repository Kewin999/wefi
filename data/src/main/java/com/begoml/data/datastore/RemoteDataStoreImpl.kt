package com.begoml.data.datastore

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Build.VERSION_CODES
import com.begoml.common.Failure
import com.begoml.common.Result
import com.begoml.common.formatDuration
import com.begoml.data.model.dto.SaveDataDto
import com.begoml.data.model.entity.SaveDataEntity
import com.google.android.gms.tasks.Tasks
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


@SuppressLint("SimpleDateFormat")
class RemoteDataStoreImpl @Inject constructor(
    private val reference: StorageReference,
    private val gson: Gson,
    private val context: Context
) : RemoteDataStore {

    private val format by lazy { SimpleDateFormat("YYYMMDD") }
    private val formatTime by lazy { SimpleDateFormat("HHmmss") }

    private val reqString: String
        get() = (Build.MANUFACTURER
                + "_" + Build.MODEL + "_" + Build.VERSION.RELEASE
                + "_" + VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name)

    override suspend fun sendData(entity: SaveDataEntity): Result<Failure, Any> {
        val time = System.currentTimeMillis()
        val date = Date(time)

        val format = format.format(date)
        val formatTime = formatTime.format(date)

        val dto = SaveDataDto(
            name = entity.name.sha256(),
            description = entity.description.sha256(),
            duration = entity.duration.formatDuration().sha256()
        )

        val json = gson.toJson(dto)
        val inputStream: InputStream = ByteArrayInputStream(json.toByteArray())


        val fileName = reqString + UUID.randomUUID().toString() + formatTime
        val newRef = reference.child("FieldData/$format/$fileName.json")

        return try {
            Tasks.await(newRef.putStream(inputStream)
                .continueWithTask { uploadTask ->
                    if (!uploadTask.isSuccessful) {
                        throw uploadTask.exception!!
                    }
                    //Now that the file is uploaded, we can retrieve the downloadUrl from the original reference
                    return@continueWithTask newRef.downloadUrl
                })
            Result.Success(Any())
        } catch (error: Exception) {
            Result.Error(Failure.NetworkFailure.UnknownHostFailure)
        }
    }
}

fun String.sha256(): String {
    return try {
        val digest = MessageDigest.getInstance("SHA-256")
        val hash: ByteArray = digest.digest(toByteArray(charset("UTF-8")))
        val hexString = StringBuffer()
        for (i in hash.indices) {
            val hex = Integer.toHexString(0xff and hash[i].toInt())
            if (hex.length == 1) hexString.append('0')
            hexString.append(hex)
        }
        hexString.toString()
    } catch (ex: Exception) {
        ""
    }
}

interface RemoteDataStore {

    suspend fun sendData(entity: SaveDataEntity): Result<Failure, Any>

}