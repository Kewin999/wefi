package com.begoml.wefi

import android.app.Application
import com.begoml.wefi.di.AppComponent
import com.begoml.wefi.di.ApplicationProvider

class BaseApp : Application() {

    private val appComponent by lazy { AppComponent.init(this@BaseApp) }

    fun getApplicationProvider(): ApplicationProvider {
        return appComponent
    }
}