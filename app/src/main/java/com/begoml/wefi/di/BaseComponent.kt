package com.begoml.wefi.di

import android.content.Context
import com.begoml.wefi.BaseApp
import com.begoml.wefi.navigation.GlobalRouter
import com.begoml.wefi.navigation.GlobalRouterImpl
import com.begoml.wefi.navigation.ScreenCreator
import com.begoml.wefi.navigation.ScreenCreatorImpl
import com.begoml.wefi.tools.ResourceProvider
import com.begoml.wefi.tools.ResourceProviderImpl
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import dagger.*
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Singleton
@Component(dependencies = [MainToolsProvider::class])
interface AppComponent : ApplicationProvider {

    companion object {

        private lateinit var component: ApplicationProvider

        fun init(app: BaseApp): AppComponent {
            val mainToolsProvider = MainToolsComponent.Initializer.init(app)

            return DaggerAppComponent.builder()
                .mainToolsProvider(mainToolsProvider)
                .build().apply {
                    component = this
                }
        }

        fun get(): ApplicationProvider {
            return component
        }
    }
}


@Singleton
@Component(
    modules = [AndroidToolsModule::class, NavigatorModule::class]
)
interface MainToolsComponent : MainToolsProvider {

    @Component.Builder
    interface Builder {

        fun build(): MainToolsComponent

        @BindsInstance
        fun appContext(context: Context): Builder
    }

    class Initializer private constructor() {
        companion object {

            fun init(app: BaseApp): MainToolsComponent = DaggerMainToolsComponent.builder()
                .appContext(app.applicationContext)
                .build()
        }
    }
}


@Module
abstract class AndroidToolsModule {

    @Binds
    @Singleton
    abstract fun bindsResourceProvider(resource: ResourceProviderImpl): ResourceProvider

    @Module
    companion object {

        @Provides
        @JvmStatic
        @Singleton
        fun provideStorageReference(context: Context): StorageReference {

            //custom
//            val options = FirebaseOptions.Builder()
//                .setApiKey("AIzaSyDXDnU8Z41YdT96mlgVUQKq76aujRCgGUI")
//                .setApplicationId("00b4903a97bc55857fd6956d500b978865ebddcc73b434dd4461f6f1a230c9a3")
//                .setStorageBucket("test_wfie")
//                .build()
//
//            val firebaseApp = FirebaseApp.initializeApp(context, options, "wefiTest")

            return FirebaseStorage.getInstance("gs://wefi-1a410.appspot.com/").reference
            //return FirebaseStorage.getInstance(firebaseApp,"gs://test_wfie").reference
        }

        @Provides
        @JvmStatic
        @Singleton
        fun provideGson(): Gson {
            return Gson()
        }
    }
}


@Module
abstract class NavigatorModule {

    @Binds
    @Singleton
    abstract fun bindsScreenCreator(screenCreator: ScreenCreatorImpl): ScreenCreator

    @Binds
    @Singleton
    abstract fun provideGlobalRouter(globalRouter: GlobalRouterImpl): GlobalRouter

    @Module
    companion object {

        private val cicerone: Cicerone<Router> = Cicerone.create()

        @JvmStatic
        @Provides
        fun provideNavigatorHolder(): NavigatorHolder = cicerone.navigatorHolder

        @JvmStatic
        @Provides
        fun provideRouter(): Router {
            return cicerone.router
        }
    }
}