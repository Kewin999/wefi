package com.begoml.wefi.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.begoml.data.datastore.CacheDataStore
import com.begoml.data.datastore.CacheDataStoreImpl
import com.begoml.data.datastore.RemoteDataStore
import com.begoml.data.datastore.RemoteDataStoreImpl
import com.begoml.data.repository.MeetingRepositoryImpl
import com.begoml.domain.repository.MeetingRepository
import com.begoml.domain.usecase.InitLocationUseCase
import com.begoml.domain.usecase.InitLocationUseCaseImpl
import com.begoml.domain.usecase.SaveDataUseCase
import com.begoml.domain.usecase.SaveDataUseCaseImpl
import com.begoml.wefi.extension.ViewModelFactory
import com.begoml.wefi.extension.ViewModelKey
import com.begoml.wefi.view.meeting.MeetingFragment
import com.begoml.wefi.view.meeting.MeetingViewModel
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [ApplicationProvider::class],
    modules = [MeetingModule::class]
)
interface MeetingComponent {

    fun inject(fr: MeetingFragment)

    companion object {

        private var component: MeetingComponent? = null

        fun init(provider: ApplicationProvider): MeetingComponent {
            return component ?: DaggerMeetingComponent
                .builder()
                .applicationProvider(provider)
                .build()
                .apply {
                    component = this
                }
        }

        fun clear() {
            component = null
        }
    }
}

@Module
abstract class MeetingModule {

    @Binds
    abstract fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MeetingViewModel::class)
    abstract fun bindsMeetingViewModel(viewModel: MeetingViewModel): ViewModel

    @Binds
    abstract fun bindsSaveDataUseCase(useCase: SaveDataUseCaseImpl): SaveDataUseCase

    @Binds
    abstract fun bindsInitLocationUseCase(useCase: InitLocationUseCaseImpl): InitLocationUseCase

    @Binds
    @Singleton
    abstract fun bindsMeetingRepository(repo: MeetingRepositoryImpl): MeetingRepository


    @Binds
    @Singleton
    abstract fun bindsRemoteDataStore(repo: RemoteDataStoreImpl): RemoteDataStore

    @Binds
    @Singleton
    abstract fun bindsChacheDataStore(repo: CacheDataStoreImpl): CacheDataStore
}