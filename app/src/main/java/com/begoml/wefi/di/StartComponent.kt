package com.begoml.wefi.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.begoml.wefi.extension.ViewModelFactory
import com.begoml.wefi.extension.ViewModelKey
import com.begoml.wefi.view.start.SingleActivity
import com.begoml.wefi.view.start.StartViewModel
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [ApplicationProvider::class],
    modules = [StartModule::class]
)
interface StartComponent {

    fun inject(activity: SingleActivity)

    companion object {

        private var component: StartComponent? = null

        fun init(provider: ApplicationProvider): StartComponent {
            return component ?: DaggerStartComponent
                .builder()
                .applicationProvider(provider)
                .build()
                .apply {
                    component = this
                }
        }

        fun clear() {
            component = null
        }
    }
}

@Module
interface StartModule {

    @Binds
    fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(StartViewModel::class)
    fun bindsStartViewModel(viewModel: StartViewModel): ViewModel
}