package com.begoml.wefi.di

import android.content.Context
import com.begoml.wefi.navigation.GlobalRouter
import com.begoml.wefi.tools.ResourceProvider
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Singleton


interface ApplicationProvider : MainToolsProvider

interface MainToolsProvider {

    fun provideContext(): Context

    @Singleton
    fun provideResourceProvider(): ResourceProvider

    @Singleton
    fun provideGlobalRouter(): GlobalRouter

    @Singleton
    fun provideNavigatorHolder(): NavigatorHolder

    @Singleton
    fun provideStorageReference(): StorageReference

    @Singleton
    fun provideGson(): Gson
}