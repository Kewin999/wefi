package com.begoml.wefi.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.begoml.wefi.extension.ViewModelFactory
import com.begoml.wefi.extension.ViewModelKey
import com.begoml.wefi.view.start.StartViewModel
import com.begoml.wefi.view.startmeeting.StartMeetingFragment
import com.begoml.wefi.view.startmeeting.StartMeetingViewModel
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [ApplicationProvider::class],
    modules = [StartMeetingsModule::class]
)
interface StartMeetingsComponent {

    fun inject(fragment: StartMeetingFragment)

    companion object {

        private var component: StartMeetingsComponent? = null

        fun init(provider: ApplicationProvider): StartMeetingsComponent {
            return component ?: DaggerStartMeetingsComponent
                .builder()
                .applicationProvider(provider)
                .build()
                .apply {
                    component = this
                }
        }

        fun clear() {
            component = null
        }
    }
}

@Module
interface StartMeetingsModule {

    @Binds
    fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(StartViewModel::class)
    fun bindsStartMeetingViewModel(viewModel: StartMeetingViewModel): ViewModel
}