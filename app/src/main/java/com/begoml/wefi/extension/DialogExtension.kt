package com.begoml.wefi.extension

import android.content.Context
import android.content.DialogInterface
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog

fun Context.showShortMessage(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun Context.showDialogWithOnButton(
    @StringRes titleRes: Int,
    @StringRes messageRes: Int,
    @StringRes buttonTitleRes: Int,
    @StringRes buttonNegativeTitleRes: Int,
    onClick: (dialogInterface: DialogInterface) -> Unit,
    onCancel: (dialogInterface: DialogInterface) -> Unit
) {
    showDialogWithOnButton(
        getString(titleRes),
        getString(messageRes),
        getString(buttonTitleRes),
        getString(buttonNegativeTitleRes),
        onClick,
        onCancel
    )
}

fun Context.showDialogWithOnButton(
    title: String,
    message: String,
    buttonTitle: String,
    buttonNegativeTitle: String,
    onClick: (dialogInterface: DialogInterface) -> Unit,
    onCancel: (dialogInterface: DialogInterface) -> Unit
) {
    AlertDialog.Builder(this).apply {
        setTitle(title)
        setMessage(message)
        setOnCancelListener {
            onCancel(it)
        }
        setPositiveButton(buttonTitle) { dialogInterface, _ ->
            dialogInterface.cancel()
            onClick(dialogInterface)
        }
        setNegativeButton(buttonNegativeTitle) { dialogInterface, _ ->
            dialogInterface.cancel()
            onCancel(dialogInterface)
        }
    }.show()
}