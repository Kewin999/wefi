package com.begoml.wefi.view.start

import android.os.Bundle
import com.begoml.wefi.navigation.GlobalRouter
import com.begoml.wefi.view.base.BaseViewModel
import javax.inject.Inject

class StartViewModel @Inject constructor(private val router: GlobalRouter) : BaseViewModel() {

    fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            router.goToStartScreen()
        }
    }
}