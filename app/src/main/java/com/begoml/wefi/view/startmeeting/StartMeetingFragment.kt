package com.begoml.wefi.view.startmeeting

import android.os.Bundle
import android.view.View
import com.begoml.wefi.BaseApp
import com.begoml.wefi.R
import com.begoml.wefi.di.StartMeetingsComponent
import com.begoml.wefi.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_start_meeting.*
import javax.inject.Inject

class StartMeetingFragment : BaseFragment(R.layout.fragment_start_meeting) {

    @Inject
    lateinit var viewModel: StartMeetingViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        context?.let {
            val provider = (it.applicationContext as BaseApp).getApplicationProvider()
            StartMeetingsComponent.init(provider).inject(this)
        }

        super.onCreate(savedInstanceState)
    }

    override fun clearComponent() {
        StartMeetingsComponent.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startMeeting.setOnClickListener {
            checkPermission(
                getString(R.string.read_state_location),
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) {
                viewModel.onStartMeetingClick()
            }
        }
    }
}