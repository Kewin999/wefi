package com.begoml.wefi.view.start

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.begoml.wefi.BaseApp
import com.begoml.wefi.R
import com.begoml.wefi.di.StartComponent
import com.begoml.wefi.extension.viewModel
import kotlinx.android.synthetic.main.activity_single.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

open class SingleActivity : AppCompatActivity(R.layout.activity_single) {


    private var instanceStateSaved: Boolean = false

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var navigator: Navigator

    private lateinit var viewModel: StartViewModel

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    override fun onCreate(savedInstanceState: Bundle?) {
        val provider = (applicationContext as BaseApp).getApplicationProvider()
        StartComponent.init(provider).inject(this)

        super.onCreate(savedInstanceState)

        navigator = SupportAppNavigator(this, R.id.fragmentContainer)

        viewModel = viewModel(viewModelFactory) {

        }

        viewModel.onCreate(savedInstanceState)
    }


    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
        instanceStateSaved = false
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    private fun clearComponent() {
        viewModel.cleared()
        StartComponent.clear()
    }


    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.fragments.firstOrNull()
        if (fragment !is BackPressed || !fragment.isBackPressed) {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isNeedClearComponent()) {
            clearComponent()
        }
    }

    open fun showLoading() {
        progressConteiner.visibility = View.VISIBLE
    }

    open fun hideLoading() {
        progressConteiner?.visibility = View.GONE
    }

    //it will be valid only for 'onDestroy()' method
    private fun isNeedClearComponent(): Boolean =
        when {
            isChangingConfigurations -> false
            isFinishing -> true
            else -> isRealRemoving()
        }

    private fun isRealRemoving(): Boolean = !instanceStateSaved
}

interface BackPressed {
    val isBackPressed: Boolean
        get() = false
}