package com.begoml.wefi.view.startmeeting

import com.begoml.wefi.navigation.GlobalRouter
import com.begoml.wefi.view.base.BaseViewModel
import javax.inject.Inject

class StartMeetingViewModel @Inject constructor(private val router: GlobalRouter) :
    BaseViewModel() {

    fun onStartMeetingClick() {
        router.goToMeetingScreen()
    }
}