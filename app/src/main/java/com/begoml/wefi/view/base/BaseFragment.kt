package com.begoml.wefi.view.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.begoml.wefi.R
import com.begoml.wefi.extension.showDialogWithOnButton
import com.begoml.wefi.view.start.BackPressed
import com.begoml.wefi.view.start.SingleActivity
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject

abstract class BaseFragment(layoutId: Int) : Fragment(layoutId), BackPressed {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var instanceStateSaved: Boolean = false
    private var permissionTags: Array<out String> = emptyArray()
    private var rationale: String = ""
    private var success: () -> Unit = {}

    override fun onResume() {
        super.onResume()
        instanceStateSaved = false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
    }

    private fun isNeedToClearComponent(): Boolean =
        when {
            activity?.isChangingConfigurations == true -> false
            activity?.isFinishing == true -> true
            else -> isRealRemoving()
        }

    private fun isRealRemoving(): Boolean =
        (isRemoving && !instanceStateSaved) || // Because isRemoving == true for fragment in backstack on screen rotation
                ((parentFragment as? BaseFragment)?.isRealRemoving() ?: false)

    @CallSuper
    override fun onDestroy() {
        super.onDestroy()
        if (isNeedToClearComponent()) {
            clearComponent()
        }
    }

    abstract fun clearComponent()

    protected fun showErrorNetworkDialog(onClick: () -> Unit, onCancel: () -> Unit) {
        context?.showDialogWithOnButton(
            R.string.error,
            R.string.server_error_message,
            R.string.try_again_server_error,
            R.string.cancel,
            { dialogInterface ->
                dialogInterface.cancel()
                onClick()
            },
            {
                onCancel()
            }
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    protected fun checkPermission(
        rationale: String,
        vararg permissionTags: String,
        success: () -> Unit
    ) {
        this.permissionTags = permissionTags
        this.success = success
        this.rationale = rationale
        requestPermissions()
    }

    @AfterPermissionGranted(1)
    private fun requestPermissions() {
        context?.let {
            if (EasyPermissions.hasPermissions(it, *permissionTags)) {
                success()
            } else {
                EasyPermissions.requestPermissions(this, rationale, 1, *permissionTags)
            }
        }
    }



    open fun showLoading() {
        view?.requestFocus()
        (activity as? SingleActivity)?.showLoading()
    }

    open fun hideLoading() {
        (activity as? SingleActivity)?.hideLoading()
    }
}