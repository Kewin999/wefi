package com.begoml.wefi.view.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

abstract class ViewStateViewModel<ViewState : Any>(state: ViewState)  : BaseViewModel() {

    private val state: MutableLiveData<ViewState> by lazy {
        MutableLiveData<ViewState>().apply {
            value = state
        }
    }

    protected fun onNextViewState(viewState: ViewState) {
        state.value = viewState
    }

    protected fun currentState() = state as LiveData<ViewState>

    protected val currentStateValue: ViewState
        get() = state.value!!

}