package com.begoml.wefi.view.meeting

import android.os.Bundle
import android.view.View
import com.begoml.wefi.BaseApp
import com.begoml.wefi.R
import com.begoml.wefi.di.MeetingComponent
import com.begoml.wefi.extension.*
import com.begoml.wefi.view.base.BaseFragment
import com.begoml.wefi.view.base.viewSateWatcher
import kotlinx.android.synthetic.main.fragment_meeting.*
import javax.inject.Inject

class MeetingFragment : BaseFragment(R.layout.fragment_meeting) {

    @Inject
    lateinit var viewModel: MeetingViewModel

    override val isBackPressed: Boolean
        get() {
            viewModel.onBackPressed()
            return true
        }

    private val viewModelWatcher = viewSateWatcher<ViewState> {

        ViewState::isLoading{
            if (it) {
                showLoading()
            } else {
                hideLoading()
            }
        }

        ViewState::dateFormat{
            counter.text = it
        }

        ViewState::isShowTimerConteiner{
            timerGroup.visibility(it)
        }

        ViewState::isShowDescriptionConteiner{
            descriptionGroup.visibility(it)
        }

        ViewState::name{
            if (it != nameEdt.text.toString()) {
                nameEdt.setText(it)
            }
        }

        ViewState::description{
            if (it != descriptionEdt.text.toString()) {
                descriptionEdt.setText(it)
            }
        }

        ViewState::isShowError{
            if (it) {
                showErrorNetworkDialog(
                    {
                        viewModel.onRetryErrorDialogClick()
                    }, {
                        viewModel.onCancelErrorDialogClick()
                    }
                )
            }
        }

        ViewState::isShowNameError{
            emptyName.visibility(it)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        context?.let {
            val provider = (it.applicationContext as BaseApp).getApplicationProvider()
            MeetingComponent.init(provider).inject(this)
        }
        super.onCreate(savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = viewModel(viewModelFactory) {
            observeViewState(viewState) {
                viewModelWatcher.render(it)
            }
        }

        stopMeeting.setOnClickListener { viewModel.onStopClick() }
        saveMeeting.setOnClickListener {
            checkPermission(
                getString(R.string.read_state_location),
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) {
                root.hideSoftKeyboard()
                viewModel.onSaveClick()
            }
        }
        cancelMeeting.setOnClickListener { viewModel.onCancelClick() }
        closeMeeting.setOnClickListener { viewModel.onCloseClick() }

        nameEdt.onChange { viewModel.onNameChanged(it) }
        descriptionEdt.onChange { viewModel.onDescriptionChanged(it) }
    }

    override fun onDestroyView() {
        viewModelWatcher.clear()
        super.onDestroyView()
    }


    override fun clearComponent() {
        viewModel.cleared()
        MeetingComponent.clear()
    }
}