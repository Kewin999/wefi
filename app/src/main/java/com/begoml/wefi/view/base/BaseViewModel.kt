package com.begoml.wefi.view.base

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseViewModel : ViewModel() {

    private val viewModelJob = Job()

    protected val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    protected val ioScope = CoroutineScope(Dispatchers.IO + viewModelJob)


    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed in scope
     */
    @CallSuper
    open fun cleared() {
        viewModelJob.cancel()

    }

}