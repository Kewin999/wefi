package com.begoml.wefi.view.meeting

import androidx.lifecycle.LiveData
import com.begoml.common.Failure
import com.begoml.common.formatDuration
import com.begoml.domain.model.SaveData
import com.begoml.domain.usecase.InitLocationUseCase
import com.begoml.domain.usecase.SaveDataUseCase
import com.begoml.wefi.navigation.GlobalRouter
import com.begoml.wefi.view.base.ViewStateViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

class MeetingViewModel @Inject constructor(
    private val router: GlobalRouter,
    private val saveDataUseCase: SaveDataUseCase,
    private val initLocationUseCase: InitLocationUseCase
) : ViewStateViewModel<ViewState>(ViewState()) {

    private lateinit var timer: Timer

    val viewState: LiveData<ViewState> by lazy {
        firstInit()
        currentState()
    }

    private fun firstInit() {
        startCounter()
        initLocationUseCase.invoke(ioScope, uiScope, Any()) {

        }
    }

    private fun startCounter() {
        timer = Timer().apply {
            schedule(100, 1000) {
                val duration = currentStateValue.duration + 1
                val dateFormat = duration.formatDuration()
                uiScope.launch {
                    onNextViewState(
                        currentStateValue.copy(
                            dateFormat = dateFormat,
                            duration = duration
                        )
                    )
                }
            }
        }
    }

    fun onStopClick() {
        timer.cancel()
        onNextViewState(
            currentStateValue.copy(
                isShowTimerConteiner = false,
                isShowDescriptionConteiner = true
            )
        )
    }

    override fun cleared() {
        super.cleared()
        timer.cancel()
    }

    fun onSaveClick() {

        if (currentStateValue.name.isEmpty()) {
            onNextViewState(currentStateValue.copy(isShowNameError = true))
            return
        }

        onNextViewState(currentStateValue.copy(isLoading = true, isShowError = false))

        val state = currentStateValue

        saveDataUseCase.invoke(
            ioScope, uiScope, SaveData(
                name = state.name,
                description = state.description,
                duration = state.duration
            )
        ) {
            it.handleResult(::handleFailure, ::handleResult)
        }
    }

    private fun handleResult(any: Any) {
        onNextViewState(currentStateValue.copy(isLoading = false))
        router.exit()
    }

    private fun handleFailure(error: Failure) {
        onNextViewState(currentStateValue.copy(isLoading = false, isShowError = true))
    }

    fun onCancelClick() {
        startCounter()
        onNextViewState(
            currentStateValue.copy(
                isShowTimerConteiner = true,
                isShowDescriptionConteiner = false,
                isShowNameError = false
            )
        )
    }

    fun onCloseClick() {
        router.exit()
    }

    fun onNameChanged(name: String) {
        onNextViewState(currentStateValue.copy(name = name, isShowNameError = false))
    }

    fun onDescriptionChanged(description: String) {
        onNextViewState(currentStateValue.copy(description = description))
    }

    fun onBackPressed() {
        router.exit()
    }

    fun onRetryErrorDialogClick() {
        onSaveClick()
    }

    fun onCancelErrorDialogClick() {
        onNextViewState(currentStateValue.copy(isShowError = false))
    }
}

data class ViewState(
    val isLoading: Boolean = false,
    val dateFormat: String = "",
    val duration: Long = 0,
    val name: String = "",
    val isShowNameError: Boolean = false,
    val description: String = "",
    val isShowTimerConteiner: Boolean = true,
    val isShowDescriptionConteiner: Boolean = false,
    val isShowError: Boolean = false
)