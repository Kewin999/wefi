package com.begoml.wefi.tools


import android.content.Context
import android.content.res.Resources
import javax.inject.Inject


/**
 * In some cases, you may want to retrieve a string or number from application resources in the Presenter or domain layer .
 * However, we know that they should not interact directly with the Android framework.
 * To solve this problem, we can create a special resource Manager entity to access external resources.
 */
class ResourceProviderImpl @Inject constructor(private val context: Context) : ResourceProvider {

    override fun getString(resId: Int): String {
        return context.getString(resId)
    }

    override fun getString(resId: Int, vararg formatArgs: Any): String {
        return context.getString(resId, *formatArgs)
    }

    override fun getResources(): Resources {
        return context.resources
    }

}