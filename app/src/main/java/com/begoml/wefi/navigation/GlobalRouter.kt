package com.begoml.wefi.navigation

interface GlobalRouter {

    fun goToStartScreen()

    fun goToMeetingScreen()

    fun exit()
}