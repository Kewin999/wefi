package com.begoml.wefi.navigation

import ru.terrakok.cicerone.Router
import javax.inject.Inject


/**
 * Can will be used for navigation and track for example analytics
 */
class GlobalRouterImpl @Inject constructor(
    private val screenCreator: ScreenCreator,
    private val router: Router
) : GlobalRouter {


    override fun goToStartScreen() {
        router.newRootChain(screenCreator.startScreen)
    }

    override fun goToMeetingScreen() {
        router.navigateTo(screenCreator.meetingScreen)
    }

    override fun exit() {
        router.exit()
    }

}