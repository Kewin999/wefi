package com.begoml.wefi.navigation

import ru.terrakok.cicerone.android.support.SupportAppScreen
import javax.inject.Inject

class ScreenCreatorImpl @Inject constructor() : ScreenCreator {

    override val startScreen: SupportAppScreen
        get() = ScreenItemImpl.Start

    override val meetingScreen: SupportAppScreen
        get() = ScreenItemImpl.Meeting
}

interface ScreenCreator {

    val startScreen: SupportAppScreen

    val meetingScreen: SupportAppScreen
}