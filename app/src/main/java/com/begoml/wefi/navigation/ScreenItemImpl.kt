package com.begoml.wefi.navigation

import androidx.fragment.app.Fragment
import com.begoml.wefi.view.meeting.MeetingFragment
import com.begoml.wefi.view.startmeeting.StartMeetingFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

sealed class ScreenItemImpl : SupportAppScreen() {

    object Start : ScreenItemImpl() {

        override fun getFragment(): Fragment = StartMeetingFragment()
    }

    object Meeting : ScreenItemImpl() {

        override fun getFragment(): Fragment = MeetingFragment()
    }
}