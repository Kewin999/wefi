package com.begoml.wefi

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.begoml.common.Failure
import com.begoml.common.Result
import com.begoml.domain.model.SaveData
import com.begoml.domain.repository.MeetingRepository
import com.begoml.domain.usecase.InitLocationUseCaseImpl
import com.begoml.domain.usecase.SaveDataUseCaseImpl
import com.begoml.wefi.navigation.GlobalRouter
import com.begoml.wefi.view.meeting.MeetingViewModel
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import kotlin.coroutines.ContinuationInterceptor

@ExperimentalCoroutinesApi
class MeetingViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: MeetingViewModel

    @Mock
    lateinit var router: GlobalRouter

    @Mock
    lateinit var repository: MeetingRepository

    @Before
    fun initViewModel() {
        MockitoAnnotations.initMocks(this)

        viewModel = MeetingViewModel(
            router = router,
            saveDataUseCase = SaveDataUseCaseImpl(
                repository
            ),
            initLocationUseCase = InitLocationUseCaseImpl(repository)
        )
    }

    @Test
    fun exit() {
        viewModel.onCloseClick()
        verify(router).exit()
    }

    @Test
    fun nameChanged() {
        val name = "alex"
        viewModel.onNameChanged(name)

        val viewState = viewModel.viewState.getOrAwaitValue(0)
        Assert.assertEquals(viewState.name, name)
    }

    @Test
    fun descriptionChanged() {
        val description = "description"
        viewModel.onDescriptionChanged(description)

        val viewState = viewModel.viewState.getOrAwaitValue(0)

        assertEquals(viewState.description, description)
    }

    @Test
    fun nameDescriptionChanged() {
        val name = "alex"
        val description = "description"
        viewModel.onDescriptionChanged(description)
        viewModel.onNameChanged(name)

        val viewState = viewModel.viewState.getOrAwaitValue(0)

        assertEquals(viewState.name, name)
        assertEquals(viewState.description, description)
    }

    @Test
    fun onSaveSuccess() {
        val name = "alex"
        val description = "description"

        viewModel.onDescriptionChanged(description)
        viewModel.onNameChanged(name)


        runBlocking {
            `when`(repository.saveData(any(SaveData::class.java)))
                .thenReturn(Result.Success(Any()))
        }

        viewModel.onSaveClick()

        val viewState = viewModel.viewState.getOrAwaitValue(0)

        assertEquals(viewState.isLoading, false)
        verify(router).exit()
    }

    @Test
    fun onSaveFailure() {
        viewModel.onNameChanged("name")
        runBlocking {
            `when`(repository.saveData(any(SaveData::class.java)))
                .thenReturn(Result.Error(Failure.NetworkFailure.NetworkConnection))
        }

        viewModel.onSaveClick()

        val viewState = viewModel.viewState.getOrAwaitValue(0)

        assertEquals(viewState.isLoading, false)
        assertEquals(viewState.isShowError, true)
    }

    private fun <T> any(type: Class<T>): T = Mockito.any<T>(type)

}

@ExperimentalCoroutinesApi
class MainCoroutineRule : TestWatcher(), TestCoroutineScope by TestCoroutineScope() {

    override fun starting(description: Description?) {
        super.starting(description)
        Dispatchers.setMain(this.coroutineContext[ContinuationInterceptor] as CoroutineDispatcher)
    }

    override fun finished(description: Description?) {
        super.finished(description)
        Dispatchers.resetMain()
    }
}