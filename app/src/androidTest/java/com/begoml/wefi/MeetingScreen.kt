package com.begoml.wefi

import android.view.View
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers
import com.begoml.wefi.utils.Waiter.waitForView
import org.hamcrest.Matcher

class MeetingScreen {

    private val stopMeeting: Matcher<View> = ViewMatchers.withId(R.id.stopMeeting)
    private val cancelMeeting: Matcher<View> = ViewMatchers.withId(R.id.cancelMeeting)

    fun clickOnStop() {
        waitForView(stopMeeting)
            .perform(click())
    }

    fun clickOnCancel() {
        waitForView(cancelMeeting)
            .perform(click())
    }
}