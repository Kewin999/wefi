package com.begoml.wefi

import androidx.test.rule.ActivityTestRule
import com.begoml.wefi.view.start.SingleActivity
import org.junit.Rule

abstract class BaseTest {

    @Rule
    @JvmField
    val activityTestRule = ActivityTestRule(SingleActivity::class.java, false, false)


}