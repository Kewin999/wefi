package com.begoml.wefi

import com.begoml.wefi.screen.StartFlowScreen
import org.junit.Test

class StartFlowTest : BaseTest() {

    @Test
    fun startMeeting() {
        activityTestRule.launchActivity(null)

        val screeStart = StartFlowScreen()
        screeStart.clickOnStartMeeting()

        val meetingScreen = MeetingScreen()
        meetingScreen.clickOnStop()

        Thread.sleep(2000)

        meetingScreen.clickOnCancel()

        Thread.sleep(3000)
    }
}