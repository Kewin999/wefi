package com.begoml.wefi.utils

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import com.azimolabs.conditionwatcher.Instruction
import junit.framework.AssertionFailedError
import org.hamcrest.Matcher

internal class IsDisplayedInstruction(private val matcher: Matcher<View>) : Instruction() {
    var error: Throwable? = null
        private set

    override fun getDescription() = "Wait for View is exist"

    override fun checkCondition(): Boolean {
        return try {
            onView(matcher).check(matches(isDisplayed()))
            true
        } catch (e: AssertionFailedError) {
            error = e
            false
        } catch (e: NoMatchingViewException) {
            error = e
            false
        }
    }
}
