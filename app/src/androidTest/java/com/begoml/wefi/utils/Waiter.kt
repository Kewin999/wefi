package com.begoml.wefi.utils

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import com.azimolabs.conditionwatcher.ConditionWatcher
import org.hamcrest.Matcher

object Waiter {
    fun waitForView(matcher: Matcher<View>): ViewInteraction {
        val instruction = IsDisplayedInstruction(matcher)
        try {
            ConditionWatcher.waitForCondition(instruction)
            return onView(matcher)
        } catch (e: Exception) {
            throw RuntimeException("View isn't displayed", instruction.error)
        }
    }
}