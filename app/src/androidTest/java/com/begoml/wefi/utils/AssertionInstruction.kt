package com.begoml.wefi.utils

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import com.azimolabs.conditionwatcher.Instruction
import org.hamcrest.Matcher

internal class AssertionInstruction(private val matcher: Matcher<View>, private val assertion: ViewAssertion) : Instruction() {
    var error: Throwable? = null
        private set

    override fun getDescription() = "Wait for Assertion"

    override fun checkCondition(): Boolean {
        return try {
            onView(matcher)
                .check(assertion)
            true
        } catch (e: NoMatchingViewException) {
            error = e
            false
        } catch (e: AssertionError) {
            error = e
            false
        }
    }
}
