package com.begoml.wefi.screen

import android.view.View
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.begoml.wefi.R
import com.begoml.wefi.utils.Waiter.waitForView
import org.hamcrest.Matcher

class StartFlowScreen {

    private val startBtn: Matcher<View> = withId(R.id.startMeeting)

    fun clickOnStartMeeting() {
        waitForView(startBtn)
            .perform(click())
    }
}